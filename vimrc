set nocompatible
filetype off

call pathogen#infect()
call pathogen#helptags()

filetype plugin indent on

"-------------- Ctrl-Z persistant ------------"
if !isdirectory($HOME."/.vim/undodir")
    call mkdir($HOME."/.vim/undodir", "", 0700)
endif
set undodir=~/.vim/undodir
set undofile

"--------------- Les racourcis ---------------"
inoremap <c-w>					<esc>:w!<CR>
inoremap <c-q>					<esc>:q!<CR>
inoremap <c-s>					<esc>:w!<CR>
noremap <c-w>					<esc>:w!<CR>
noremap <c-q>					<esc>:q!<CR>
noremap <c-s>					<esc>:w!<CR>
"map <F5> 						:call CompileRun()<CR>
"imap <F5>				 		<Esc>:call CompileRun()<CR>
"vmap <F5> 						<Esc>:call CompileRun()<CR>
"map <F6>						:call RunVSCode()<CR>
"imap <F6>						<Esc>:call RunVSCode()<CR>
"vmap <F6>						<Esc>:call RunVSCode()<CR>
nmap <F8>						:TagbarToggle<CR>
noremap <S-o>					:Stdheader<CR>
noremap <S-n>					:!(norminette)<CR>
noremap <S-m>					:r $HOME/main.template<CR>
noremap <C-d>					:vs 
noremap <S-d>					:split 
noremap <S-Right>				<C-w><Right>
noremap <S-Left>				<C-w><Left>
noremap <S-Up>					<C-w><Up>
noremap <S-Down>				<C-w><Down>
inoremap <TAB>					<TAB>
noremap <C-k>					:!make ; (make run)<CR>
noremap <C-e>					:!gcc *.c -ggdb ; (time ./a.out)<CR>
imap <C-g>						<esc>:NERDTreeToggle<CR>
map <C-g>						:NERDTreeToggle<CR>

"--------------- Snippets ---------------------------"
let g:UltiSnipsExpandTrigger	="<F2>"

"--------------- utilitaires basiques ---------------"
syntax 	on
set 	mouse=a
set 	cursorline
set 	nu
set 	tabstop=4
set 	shiftwidth=4
set 	smartindent
set 	autoindent
set 	shiftround
set 	showmode
set 	backspace=indent,eol,start
set 	pumheight=50
set		whichwrap+=<,>,[,]
set 	encoding=utf-8
au 		BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif

"---------- Qui utilise la scrollbar -----------"
set 	guioptions-=r
set 	guioptions-=R
set 	guioptions-=l
set 	guioptions-=L

"-------- Auto resize quickfix loclist --------"
au FileType qf call AdjustWindowHeight(3, 10)
au FileType qf call ToggleLocList()
aug QFClose
  au!
  au WinEnter * if winnr('$') == 1 && &buftype == "quickfix"|q|endif
aug END

"--------------- jeu de couleur ---------------"
colorscheme 	gruvbox
set background	=dark
set t_Co		=256
let g:lightline = {
      \ 'colorscheme': 'gruvbox',
      \ 'active': {
      \   'left': [ ['mode', 'paste'],
      \             ['fugitive', 'readonly', 'filename', 'modified'] ],
      \   'right': [ [ 'lineinfo' ], ['percent'] ]
      \ },
      \ 'component': {
      \   'readonly': '%{&filetype=="help"?"":&readonly?"🔒":""}',
      \   'modified': '%{&filetype=="help"?"":&modified?"+":&modifiable?"":"-"}',
      \   'fugitive': '%{exists("*FugitiveHead")?FugitiveHead():""}'
      \ },
      \ 'component_visible_condition': {
      \   'readonly': '(&filetype!="help"&& &readonly)',
      \   'modified': '(&filetype!="help"&&(&modified||!&modifiable))',
      \   'fugitive': '(exists("*FugitiveHead") && ""!=FugitiveHead())'
      \ },
      \ 'separator': { 'left': ' ', 'right': ' ' },
      \ 'subseparator': { 'left': ' ', 'right': ' ' }
      \ }

"-------------- Auto Pairs ---------------------"
let g:AutoPairsFlyMode 			= 0
let g:AutoPairsMapCR 			= 0
let g:AutoPairsWildClosedPair 	= ''
let g:AutoPairsMultilineClose 	= 0
imap <silent><CR>				<CR><Plug>AutoPairsReturn

"--------------- Vimspector  -------------------"
let g:vimspector_enable_mappings = 'HUMAN'

"--------------- CLANG COMPLETER ---------------"
set noinfercase
set completeopt+=preview
set completeopt+=menuone,noselect
let g:clang_library_path='/usr/lib/x86_64-linux-gnu/libclang-11.so.1'
let g:clang_complete_auto = 1
let g:mucomplete#enable_auto_at_startup = 1
"
"--------------- TAGBAR ---------------"
let g:tagbar_ctags_bin="/usr/local/bin/ctags"

"---------------- Ale --------------------"
let g:ale_lint_on_text_changed='always'
let g:ale_lint_on_enter = 1
let g:ale_lint_on_save = 0
let g:ale_set_loclist = 0
let g:ale_set_quickfix = 1
let g:ale_open_list = 1
let g:ale_keep_list_window_open = 1
let g:ale_lint_delay = 200

"---------------  Syntastic  ---------------"
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*
let g:syntastic_c_checkers = ['norminette']
let g:syntastic_aggregate_errors = 1
let g:syntastic_c_norminette_exec = 'norminette'

let g:c_syntax_for_h = 1
let g:syntastic_c_include_dirs = ['include', '../include', '../../include', 'libft', '../libft/include', '../../libft/include']
"let g:syntastic_c_norminette_args = '-R CheckTopCommentHeader'
let g:syntastic_check_on_open = 0 
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_wq = 0

"--------------- PL NERDTREE ---------------"
let sbv_open_nerdtree_to_start=1
let sbv_open_nerdtree_with_new_tab=1
autocmd BufCreate * call s:addingNewTab(sbv_open_nerdtree_with_new_tab)
autocmd VimEnter * call s:actionForOpen(sbv_open_nerdtree_to_start)
autocmd WinEnter * call s:CloseIfOnlyNerdTreeLeft()

"--------------- FONCTION ---------------"
function! AdjustWindowHeight(minheight, maxheight)
  exe max([min([line("$"), a:maxheight]), a:minheight]) . "wincmd _"
endfunction

function! ToggleLocList()
	if exists("g:SyntasticLoclist")
		if !exists("g:qfix_win")
			copen
			let g:qfix_win = bufnr("$")
		endif
	endif
endfunction

function! s:actionForOpen(openNerdTree)
	let filename = expand('%:t')
	if !empty(a:openNerdTree)
		NERDTree
	endif
	if !empty(filename)
		wincmd l
	endif
endfunction

function! s:addingNewTab(openNerdTree)
	let filename = expand('%:t')
	if winnr('$') < 2 && exists('t:NERDTreeBufName') == 0
		if !empty(a:openNerdTree)
			NERDTree
		endif
		if !empty(filename)
			wincmd l
		endif
	endif
endfunction

function! s:CloseIfOnlyNerdTreeLeft()
	if exists("t:NERDTreeBufName")
		if exists("g:qfix_win")
			if bufwinnr(t:NERDTreeBufName) != -1
				if winnr("$") == 2
					q
				endif
			endif
		endif
	endif
endfunction

func! CompileRun()
exec "w"
if &filetype == 'c'
	exec "!gcc % -o %< && time ./%<"
elseif &filetype == 'cpp'
    exec "!g++ % -o %<"
    exec "!time ./%<"
elseif &filetype == 'java'
    exec "!javac %"
    exec "!time java %"
elseif &filetype == 'sh'
    exec "!time bash %"
elseif &filetype == 'python'
    exec "!time python3 %"
elseif &filetype == 'html'
    exec "!google-chrome % &"
elseif &filetype == 'go'
    exec "!go build %<"
    exec "!time go run %"
elseif &filetype == 'matlab'
    exec "!time octave %"
elseif &filetype == 'vala'
	exec "!valac %"
elseif &filetype == 'vapi'
	exec "!valac % -o %< && time ./%<"
endif
endfunc

func! RunVSCode()
exec "w"
if !isdirectory(/tmp/vscode)
    call mkdir(/tmp/vscode, "", 0700)
endif
exec "!rm -rf /tmp/vscode/*"
if &filetype == 'c'
	exec "!cp % /tmp/vscode/tmp.c && code /tmp/vscode/tmp.c"
endif
endfunc

