**SupraVim**

L'editeur de texte vim pour 42 Angoulême.

<img src="readme.png"/>

| Dépendance |
| ------ |
| ZSH |
| clang |
| Vim 8 |

**Installation:**

```bash
curl https://gitlab.com/clinche/SupraVim/-/raw/master/installer.sh | sh
```

**Désinstallation:**

```bash
    rm -rf ~/.local/bin/SupraVim
```


| Nom | Raccourcis |
| ------ | ------ |
| Compilation |Ctrl+E|
| HEADER 42| **F1**|
| Deplacement | Shift - flèche|
| Sauvegarde | Ctrl + S ou Ctrl + W |
| Quitter | Ctrl + Q |
| Fermer la fenêtre des dossiers | Ctrl +G |
| MakeFile| Ctrl+K (lance make et make run)|
| scinder la fenetre horizontalement | Ctrl + D|
| scinder la fenetre verticalement | Shift + D|

**Raccourcis Debug**

<img src="debugging.png"/>

Appuyer sur `<F5>` pour lancer le debug du fichier a.out. Si le fichier s'appelle autrement et/ou si le fichier n'est pas un fichier C, rajouter un fichier de configuration 
vim/bundle/vimspector/configurations/`<OS>`/`<language>`/default.json
Des configurations exemples sont disponibles dans vim/bundle/vimspector/.vimspector.json

| Key          | Mapping                                       | Function
| ---          | ---                                           | ---
| `F5`         | `<Plug>VimspectorContinue`                    | When debugging, continue. Otherwise start debugging.
| `F3`         | `<Plug>VimspectorStop`                        | Stop debugging.
| `F4`         | `<Plug>VimspectorRestart`                     | Restart debugging with the same configuration.
| `F6`         | `<Plug>VimspectorPause`                       | Pause debuggee.
| `F9`         | `<Plug>VimspectorToggleBreakpoint`            | Toggle line breakpoint on the current line.
| `<leader>F9` | `<Plug>VimspectorToggleConditionalBreakpoint` | Toggle conditional line breakpoint or logpoint on the current line.
| `F8`         | `<Plug>VimspectorAddFunctionBreakpoint`       | Add a function breakpoint for the expression under cursor
| `<leader>F8` | `<Plug>VimspectorRunToCursor`                 | Run to Cursor
| `F10`        | `<Plug>VimspectorStepOver`                    | Step Over
| `F11`        | `<Plug>VimspectorStepInto`                    | Step Into
| `F12`        | `<Plug>VimspectorStepOut`                     | Step out of current function scope


Prend en charge la coloration de:
- python
- vala
- C
- C++
- Shell
